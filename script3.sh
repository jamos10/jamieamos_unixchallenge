#!/bin/bash

egrep 'Record Publish|Type: Trade|wTradeVolume|wTradePrice' /var/tmp/opra_example_regression.log |
grep -A2 -B1 "Type: Trade" |
sed -e 's/Regression: //' -e '/^--/d' |
sed '/Record Publish/N;s/\n//;P;D' > Record



cat Record
echo "____________________________________________"
echo "Total Trade Price: "
grep 'wTradePrice' Record | awk -F '|' '{price+=$4} END{print price;}'
echo "Total Trade Vol: " 
grep "wTradeVolume" Record | awk -F '|' '{volume+=$4} END{print volume;}'
echo "____________________________________________"
for symbolx in `grep 'Record Publish' Record | awk '{print $3, $4}' | sed 's/ /:/' | sort | uniq`
do
symbol=$(echo "$symbolx" | sed 's/:/ /')
grep -A2 "$symbol" Record | awk "BEGIN {
		tradePrice=0;
		tradeVolume=0;
	}
		\$1 ~ /wTradePrice/{
		tradePrice+=\$7;
		}
		\$1 ~ /wTradeVolume/{
		tradeVolume+=\$7;
		}						 
	END{

print \"$symbol\";
print \" Trade Price =\" tradePrice;
print \" Trade Volume =\" tradeVolume;
}"
done
